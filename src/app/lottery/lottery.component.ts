import {Component} from '@angular/core';

@Component({
  selector: 'app-lottery',
  templateUrl: './lottery.component.html',
  styleUrls: ['./lottery.component.css']
})
export class LotteryComponent {
  numbers: number[] = [];

  constructor() {
    this.numbers = [0,0,0,0,0];
  }

  generateNumber() {
    this.numbers = [];
    while(this.numbers.length < 5){
      let randomNumber = Math.floor(Math.random() * (36 - 5) + 5) + 1;
      if(this.numbers.indexOf(randomNumber) === -1) this.numbers.push(randomNumber);
    }
    return this.numbers = [...this.numbers].sort((a,b)=>a-b);
  }
}

