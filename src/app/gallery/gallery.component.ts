import { Component } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  passwordMatrix = '1111';
  password = '';
  showAddImgForm = false;
  imgTitle = '';
  imgUrl = '';
  images = [
    {id: 1,title: 'Шлакоблок',url: 'https://4tololo.ru/sites/default/files/inline/images/2019/10/29-1424-342327597.jpg'},
    {id: 2,title: 'Лёня Василич',url: 'https://i.pinimg.com/originals/42/71/27/4271274535e842c95bea937ea7003bef.jpg'},
  ];

  constructor() {
    type Image = {
      id: number,
      title: string;
      url: string;
    }
  }

  // password
  onPasswordInputChange(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.password = target.value;
  }

  formPasswordIsEmpty() {
    return this.password === '';
  }

  onPasswordSubmit(event: Event) {
    event.preventDefault();
    if (this.password === this.passwordMatrix) {
      this.showAddImgForm = true;
    }
  }

  formAddImgIsEmpty() {
    return this.imgTitle === '' || this.imgUrl === '';
  }

  onAddImage(event: Event) {
    event.preventDefault();

    let id = 1;
    let last = this.images.slice(-1).pop();
    if (last === undefined) {
      id = 1;
    } else {
      id = last.id + 1;
    }

    this.images.push({
      id: id,
      title: this.imgTitle,
      url: this.imgUrl
    })
    this.imgTitle = '';
    this.imgUrl = '';
  }

  deleteImage(image_id: number) {
    this.images = this.images.filter(({ id }) => id !== image_id);
  }

}
